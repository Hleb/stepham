window.onload = function() {
  document.querySelector(`.servicesMenuNav`).addEventListener(`click`, menuChange);
  function menuChange(event) {
    if (event.target.className == `servicesMenuItem`) {
      const tabindex = event.target.getAttribute(`tabindex`);
      const smi = document.getElementsByClassName(`servicesMenuItem`);
      for (let i = 0; i < smi.length; i++) {
        smi[i].classList.remove(`active`);
      }
      event.target.classList.add(`active`);
      const mic = document.getElementsByClassName(`MenuItemContent`);
      for (let i = 0; i < mic.length; i++) {
        if (tabindex == i) {
          mic[i].style.display = `flex`;
          document.getElementsByClassName('MenuItemContentSpecial')[0].style.display = `none`;
        } else {
          mic[i].style.display = `none`;
        }
      }
    }
  }
};

const filterButtons = document.querySelectorAll(`.filterBarItem`);
const addBtn = document.querySelector(`.addBtn`);
const filteredImages = document.querySelector(`.filteredImages`);
let counter = 0;
function gen(i) {
  if (i <= 3) {
    return 1;
  }
  if (i <= 6) {
    return 2;
  }
  if (i <= 9) {
    return 3;
  }
  if (i <= 12) {
    return 4;
  }
}
function filterTabindex(arr, tabindex) {
  arr.forEach(function(k) {
    if (tabindex !== k.getAttribute(`tabindex`)) {
      k.style.display = `none`;
    } else {
      k.style.display = `flex`;
    }
    if (tabindex === `0`) {
      k.style.display = `flex`;
    }
  });
}
function add12Pictures() {
  counter++;
  for (let i = 0; i < 12; i++) {
    const block = `<div class="filteredImage" tabindex="${gen(
  i + 1)}"><img class="importantImage" src="https://picsum.photos/286/211/?image=${counter *
      (i + 20)}" alt="a"><div class="filterHover">
        <div class="hover-icons">
        <div class="hover-icon"><i class="fas fa-search"></i></div>
        <div class="hover-icon"><i class="fas fa-link"></i></div>
        </div>
        <div class="hover-title">
        <p class="hover-heading">Creative design</p>
        <p class="hover-subheading">Web design</p>
        </div>
      </div>
      </div>

      `;
    filteredImages.innerHTML += block;
  }
  if (counter >= 3) {
    addBtn.style.display = `none`;
  }
  const chosenBtn = document.querySelector(`.fbiAct`).getAttribute(`tabindex`);
  const imgArr = document.querySelectorAll('.filteredImage');
  filterTabindex(imgArr, chosenBtn);
}
add12Pictures();
function ourWorksFilter(event) {
  document.querySelector('.fbiAct').classList.remove(`fbiAct`);
  event.target.classList.add('fbiAct');
  const tabindex = event.target.getAttribute(`tabindex`);
  const imgArray = document.querySelectorAll(`.filteredImage`);
  filterTabindex(imgArray, tabindex);
}

filterButtons.forEach(function(i) {
  i.addEventListener(`click`, ourWorksFilter);
});
addBtn.addEventListener(`click`, add12Pictures);


$(document).ready(function(){
  $('.chosenSection').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: true,
    asNavFor: '.sliderNavItems',
    cssEase: 'linear',
    infinite: false
  });
  $('.sliderNavItems').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: true,
    infinite: false
  });
});
